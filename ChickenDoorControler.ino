#include <MecLogging.h>
#include <MecWifi.h>
#include <MecWebThing.h>
 
#include <time.h>
//#include <TimeLib.h> //https://github.com/PaulStoffregen/Time

/**
 * ****************************************************************************
 *  GLOBALS
 * ****************************************************************************
 */
//int greenLedPin = D7; // Green LED
const int redLedPin = D0; // Red LED
const int analogInPin = A0;

double timedif = 0;            // if no NTC signal, this is the diffrence from the time value.
bool ChickenDoorStatus = true; // is door open or closed

int ChickenDoorOpenPin = D2;
int ChickenDoorClosePin = D3;

int OpenTimeDoor = 60;

int closetime = -1;

int skipADoodleTimer = 0;

/**
 * ****************************************************************************
 *  Setting up Debug
 * ****************************************************************************
 */
// LED to flash when there is activity
#define ActivityLedMode Normal // None, Inverse or Normal
// Debug level
//    - Inf for "production"
//    - Deb for "debug/development"
//    see libary Debug.h for all the debug levels
#define MyLogLevel Deb

MecLogging MyLog(MyLogLevel, redLedPin, ActivityLedMode);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */
String WiFiSSID[] = {
    "Clemens 5n",
    "Michaels phone",
    "Clemens 5nn",
    "Clemens 5g"};
String WiFiPassword[] = {"zurtkeld",
                         "zurtkeld",
                         "zurtkeld",
                         "zurtkeld"};
int WifiCount = 4;

String name = "ChickenDoorControler";

MecWifi MyWifi(WiFiSSID, WiFiPassword, WifiCount, &MyLog, name);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */

MecThing Thing(name, "ChickenDoorControler", "ChickenDoorControler for Clemens 5", &MyLog);

MecProperty PropAlarm(&MyLog, "Alarm", "Alarm", "Alarm ved overbelastning", false, true, TypeBoolean);

MecProperty PropOpen(&MyLog, "onoff", "Open/Close ChickenDoor", "Åben eller luk for hønselemmen", true);

MecProperty PropTimeHH(&MyLog, "timeHH", "Curent time, Hour", "Tid, Time", 9, false, "time", 0, 23, TypeInteger);
MecProperty PropTimeMM(&MyLog, "timeMM", "Curent time, Minnutes", "Tid, Minutter", 31, false, "Minuttes", 0, 59, TypeInteger);

MecProperty PropOpenHH(&MyLog, "openHH", "Opening time, Hour", "Åbenetid, Time", 8, false, "time", 0, 23, TypeInteger);
MecProperty PropOpenMM(&MyLog, "openMM", "Opening time, Minnutes", "Åbnetid, Minutter", 0, false, "Minuttes", 0, 59, TypeInteger);

MecProperty PropCloseHH(&MyLog, "closeHH", "Closeing time, Hour", "Lukketid, Time", 22, false, "time", 0, 23, TypeInteger);
MecProperty PropCloseMM(&MyLog, "closeMM", "Closeing time, Minnutes", "Lukketid, Minutter", 0, false, "Minuttes", 0, 59, TypeInteger);

MecProperty PropOpenFactor(&MyLog, "openFactor", "OpenDoorTimerFactor", "OpenDoorTimerFactor", 1006, false, "Promille", 800, 1200, TypeInteger);
MecProperty PropAjustDoor(&MyLog, "AjustDoor", "Ajust door position", "Positive for up / Negative for down", 0, false, "milliseconds", -50, 50, TypeInteger);

/**
 * ****************************************************************************
 * ****************************************************************************
 *  
 * ****************************************************************************
 */

/**
 * ****************************************************************************
 * ****************************************************************************
 *  setup
 * ****************************************************************************
 */
void setup()
{
  // Init libaries
  MyLog.ActivityLED(LedON);
  MyLog.setup();
  MyWifi.setup();

  Thing.addProperty(&PropAlarm);
  Thing.addProperty(&PropOpen);
  Thing.addProperty(&PropTimeHH);
  Thing.addProperty(&PropTimeMM);
  Thing.addProperty(&PropOpenHH);
  Thing.addProperty(&PropOpenMM);
  Thing.addProperty(&PropCloseHH);
  Thing.addProperty(&PropCloseMM);
  Thing.addProperty(&PropOpenFactor);
  Thing.addProperty(&PropAjustDoor);
  Thing.setup();

  // Setup Tid
  configTime(1 * 3600, 1 * 3600, "pool.ntp.org", "time.nist.gov");

  MyLog.infoNl("Setup- start...", Deb);
  pinMode(ChickenDoorOpenPin, OUTPUT);
  pinMode(ChickenDoorClosePin, OUTPUT);
  digitalWrite(ChickenDoorOpenPin, HIGH);
  digitalWrite(ChickenDoorClosePin, HIGH);

  // publish end setup
  MyLog.infoNl("Setup- End...", Deb);
  MyLog.ActivityLED(LedOFF);
}

void OpenChickenDoor(bool _opendoor)
{
  if (PropAlarm.GetBooleanValue())
  {
    MyLog.infoNl("ChickenDoor: Alarmtilstand, dør åbnes eller lukkes ikke", War);
  }
  else
  {
    if (_opendoor == ChickenDoorStatus)
    {
      //MyLog.infoNl("ChickenDoor: Ingen aendring", Inf);
    }
    else
    {
      if (_opendoor == true) // Open
      {
        MyLog.infoNl("ChickenDoor OPEN.", Inf);
        digitalWrite(ChickenDoorOpenPin, LOW);
        digitalWrite(ChickenDoorClosePin, HIGH);
        closetime = millis() + OpenTimeDoor * 100 * PropOpenFactor.GetIntegerValue() / 1000;
      }
      else
      {
        MyLog.infoNl("ChickenDoor Down", Inf);
        digitalWrite(ChickenDoorOpenPin, HIGH);
        digitalWrite(ChickenDoorClosePin, LOW);
        closetime = millis() + OpenTimeDoor * 100;
      }
      delay(500);
      ChickenDoorStatus = _opendoor;
    }
  }
};

void AjustDoor(int t)
{
  MyLog.infoNl("ChickenDoor Ajusting: " + String(t), Inf);
  PropAlarm.SetValue(false);

  if (t > 0)
  {
    digitalWrite(ChickenDoorOpenPin, LOW);
    digitalWrite(ChickenDoorClosePin, HIGH);
    delay(t * 100);
  }
  else
  {
    digitalWrite(ChickenDoorOpenPin, HIGH);
    digitalWrite(ChickenDoorClosePin, LOW);
    delay(t * -100);
  }

  digitalWrite(ChickenDoorOpenPin, HIGH);
  digitalWrite(ChickenDoorClosePin, HIGH);
  if (ChickenDoorStatus == true)
  {
    OpenTimeDoor = OpenTimeDoor + t;
  }
}

/**
 * ****************************************************************************
 * ****************************************************************************
 *  loop
 * ****************************************************************************
 */
void loop()
{

  // Loop libaries
  MyLog.ActivityLED(LedON);
  MyWifi.loop();
  MyLog.loop();
  Thing.loop();

  // put your main code here, to run repeatedly:

  if (skipADoodleTimer < millis())
  {
    time_t now;
    struct tm *timeinfo;
    time(&now);

    skipADoodleTimer = millis() + 500;

    if (now > 999999999)
    {
      // alså vi har fået et opdateret NTP Tid, så det er den vi bruger.
      //MyLog.infoNl("Loop, har NTP Tid: " + String(timedif), Deb);
      timedif = 0;
    }
    else
    {
      //MyLog.infoNl("Loop, har IKKE NTP Tid: " + String(timedif), Deb);
      if (timedif == 0 || PropTimeHH.IsChanged() || PropTimeMM.IsChanged())
      {
        timedif = PropTimeHH.GetIntegerValue() * 60 * 60 + PropTimeMM.GetIntegerValue() * 60 - now;
        MyLog.infoNl("Loop, Beregner Diff til: " + String(timedif), Deb);
      }
      now = now + timedif;
    }

    timeinfo = localtime(&now);
    PropTimeHH.SetValue(timeinfo->tm_hour);
    PropTimeMM.SetValue(timeinfo->tm_min);

    if (PropTimeHH.GetIntegerValue() == PropOpenHH.GetIntegerValue() && PropTimeMM.GetIntegerValue() == PropOpenMM.GetIntegerValue())
    {
      PropOpen.SetValue(true, _ChangedFromRemote);
      MyLog.infoNl("Loop, Timer opening door at " + String(PropOpenHH.GetIntegerValue()) + ":" + String(PropOpenMM.GetIntegerValue()), Deb);
    }

    if (PropTimeHH.GetIntegerValue() == PropCloseHH.GetIntegerValue() && PropTimeMM.GetIntegerValue() == PropCloseMM.GetIntegerValue())
    {

      PropOpen.SetValue(false, _ChangedFromRemote);
      MyLog.infoNl("Loop, Timer closing door at" + String(PropCloseHH.GetIntegerValue()) + ":" + String(PropCloseMM.GetIntegerValue()), Deb);
    }

    if (PropOpen.IsChanged())
    {
      OpenChickenDoor(PropOpen.GetBooleanValue());
    }

    if (PropAjustDoor.IsChanged())
    {
      AjustDoor(PropAjustDoor.GetIntegerValue());
      PropAjustDoor.SetValue(0);
    }
    int sensorValue = analogRead(analogInPin);

    if (sensorValue > 0 && closetime != -1)
    {
      MyLog.infoAppend(" A: " + String(sensorValue), Inf);
    }

    if (sensorValue > 10)
    {
      MyLog.infoNl("ALARM ALARM, High Load (" + String(sensorValue) + "), Stoping setting alarm, stopping motor", Sto);
      digitalWrite(ChickenDoorOpenPin, HIGH);
      digitalWrite(ChickenDoorClosePin, HIGH);
      PropAlarm.SetValue(true);
      closetime = -1;
    }
  }

  if (closetime < millis() && closetime != -1)
  {
    MyLog.infoNl("ChickenDoor Stop", Inf);
    digitalWrite(ChickenDoorOpenPin, HIGH);
    digitalWrite(ChickenDoorClosePin, HIGH);
    closetime = -1;
  }

  //MyLog.infoNl("Antal AP:" + String(WiFi.softAPgetStationNum()), Deb);
  // publish end loop
  MyLog.ActivityLED(LedOFF);
}
